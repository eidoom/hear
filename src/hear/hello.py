from flask import Flask

app = Flask(__name__)


@app.route("/api/test")
def api():
    return {
        "artist": "An artist",
        "title": "A title",
    }
