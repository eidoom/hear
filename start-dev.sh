#!/usr/bin/env bash

export FLASK_APP=src/hear/hello.py
export FLASK_ENV=development

python3 -m flask run --host=0.0.0.0
